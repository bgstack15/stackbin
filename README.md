<!--
  -- Filename: README.md
  -- Startdate: 2022-02-11
  -->
# Readme for stackbin
This project is a hard fork of a flask-based pastebin solution, intended for use on my production systems.

## Upstream
[gitlab](https://gitlab.com/bgstack15/stackbin)

## Features

* Admin page which can list parents, children, and provide link to delete pastes.
* Editable titles
* "Reply to" pastes to make parent/children relationships
* UUIDs instead of sequential integer ID numbers
* Private pastes (accessible to admin, and to users with the whole link)
* Reverse-proxy autoconfiguration by visiting `/set`

## Using stackbin
### Installing

You can use flask for development servers, and uwsgi for production.

### Instructions

Configure the application with these two files, based on the `.example` files available in the source code:

* stackbin.conf
* stackbin.wsgi.ini

#### Development
Run server in development mode.

    FLASK_APP=stackbin.py FLASK_DEBUG=True flask run --host='0.0.0.0'

#### Production
Run the server in a full wsgi environment for the cleanup timer to operate.

    ./stackbin.bin

If you use stackbin behind a reverse-proxy such as nginx with example file `stackbin.conf.nginx`, then you can have it autodetect the correct top-level path by visiting path:

    /set

This means that if your app is behind `http://example.com/stackbin/` then you would just visit once page:

    http://example.com/stackbin/set

### Command line

The application is designed primarily to be used with the web form, however, users can send data from the command line with curl.

    $ apt-cache show putty | curl https://example.com/stackbin/ -X POST --data-raw '@-'
    https://example.com/stackbin/a2ee1780-f4c3-4672-bd47-e6ff0334b215

## Dependencies
 For a production stack on CentOS 7:

    yum install nginx uwsgi uwsgi-logger-file python36-flask uwsgi-plugin-python36 python36-sqlalchemy python36-uwsgidecorators
    pip3 install --user flask-sqlalchemy pytimeparse

## Improvements
None at this time.

## Alternatives
This is a very diverged fork of [su27/flask-pastebin](https://github.com/su27/flask-pastebin) which itself was a fork of the original [mitsuhiko/pastebin](https://github.com/mitsuhiko/flask-pastebin). The original had a few additional features worth reviewing.

### Unresearched
https://github.com/yasoob/logit-bin
https://github.com/AWilliams17/PasteMate
https://github.com/bsamadi/flask-pastebin

### Attempted
https://github.com/Tygs/0bin sounds cool but it uses a stack I'm unfamiliar with and it had some issues and I didn't want to bother with it.

## References

1. Using UUIDs instead of integers in sqlite in SQLAlchemy: [https://stackoverflow.com/questions/183042/how-can-i-use-uuids-in-sqlalchemy/812363#812363](https://stackoverflow.com/questions/183042/how-can-i-use-uuids-in-sqlalchemy/812363#812363)
2. https://stackoverflow.com/questions/15231359/split-python-flask-app-into-multiple-files/15231623#15231623
3. https://stackoverflow.com/questions/18214612/how-to-access-app-config-in-a-blueprint/38262792#38262792
