# Apache example config for stackbin application
# Needs setsebool -P http_can_network_connect 1
# vim:set syntax=apache ts=3 sw=3 sts=3 sr et:
<VirtualHost *:80>

   ServerName d2-03a.ipa.example.com

   ServerAdmin webmaster@localhost
   DocumentRoot /var/www/html

   #LogLevel info ssl:warn

   ErrorLog ${APACHE_LOG_DIR}/error.log
   CustomLog ${APACHE_LOG_DIR}/access.log combined

	# OPTION 1: send to https
   # force https for this path
   RewriteEngine On
   RewriteCond %{HTTPS} !=on
   RewriteCond %{HTTP_HOST} !^(localhost|127.0.0.1)
   RewriteRule ^/stackbin(.*) https://%{SERVER_NAME}/stackbin$1 [R,L]

	# OPTION 2: Just use unencrypted
   #ProxyPass /stackbin http://localhost:4680/
   #ProxyPassReverse /stackbin http://localhost:4680/
   #<Location /stackbin>
   #   RequestHeader append X-Forwarded-Prefix "/stackbin"
   #   RequestHeader set X-Forwarded-Proto "http"
   #</Location>

</VirtualHost>

# To use OPTION 2 above, just disable this whole 443 virtualhost.
<VirtualHost *:443>
   ServerName d2-03a.ipa.example.com

   ServerAdmin webmaster@localhost
   DocumentRoot /var/www/html

   #LogLevel info ssl:warn

   ErrorLog ${APACHE_LOG_DIR}/ssl-error.log
   CustomLog ${APACHE_LOG_DIR}/ssl-access.log combined

   SSLEngine on
   SSLProtocol all -SSLv2 -SSLv3
   SSLHonorCipherOrder on
   SSLCipherSuite "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA !RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS"

   SSLCertificateFile /etc/ssl/private/https-d2-03a.ipa.example.com.pem
   SSLCertificateKeyFile /etc/ssl/private/https-d2-03a.ipa.example.com-nopw.key

   ProxyPass /stackbin http://localhost:4680/
   ProxyPassReverse /stackbin http://localhost:4680/
   <Location /stackbin>
      # a2enmod headers. These are extra ones that are not provided by Apache natively.
      RequestHeader set X-Forwarded-Proto "https"
      # This header is not required to be set manually. The ProxyPass orand Location directive already provide it!
      #RequestHeader append X-Forwarded-Prefix "/stackbin"
   </Location>

</VirtualHost>
